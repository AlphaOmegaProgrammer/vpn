#include <sys/socket.h>


#define TUN_QUEUE_SIZE	64

#define TUN_MTU			65535

struct globals {
	int tun_fds[TUN_QUEUE_SIZE], vpn_fd;
	unsigned short mtu;
};
