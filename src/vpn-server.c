#include <arpa/inet.h>
#include <errno.h>
#include <fcntl.h>
#include <linux/if.h>
#include <linux/if_tun.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>


#include "vpn.h"


#define VPN_LISTEN_HOST	"10.255.255.3"
#define VPN_LISTEN_PORT	12345

#define VPN_SERVER_IP	"10.255.255.3"


void cleanup(struct globals *globals){
	close(globals->vpn_fd);

	unsigned long i;
	for(i=0; i<TUN_QUEUE_SIZE; i++)
		close(globals->tun_fds[i]);
}

int setup(struct globals *globals){
	struct ifreq vpn_ifr = {0};
	unsigned long i;
	int result;

	vpn_ifr.ifr_flags = IFF_TUN | IFF_MULTI_QUEUE;
	for(i=0; i<TUN_QUEUE_SIZE; i++){
		int fd = open("/dev/net/tun", O_RDWR);
		if(fd < 0){
			perror("Failed to open generic TUN fd");
			return fd;
		}

		result = ioctl(fd, TUNSETIFF, &vpn_ifr);
		if(result < 0){
			perror("ioctl TUNSETIFF failed");
			return result;
		}

		globals->tun_fds[i] = fd;
	}

	globals->vpn_fd = socket(AF_INET, SOCK_STREAM, 0);
	if(globals->vpn_fd < 0){
		perror("Failed to open new TUN fd");
		return globals->vpn_fd;
	}

	vpn_ifr.ifr_addr.sa_family = AF_INET;
	result = inet_pton(AF_INET, VPN_SERVER_IP, vpn_ifr.ifr_addr.sa_data + sizeof(short));
	if(result < 1){
		perror("inet_pton "VPN_SERVER_IP" failed");
		return result;
	}

	result = ioctl(globals->vpn_fd, SIOCSIFADDR, &vpn_ifr);
	if(result < 0){
		perror("ioctl SIOCSIFADDR failed");
		return result;
	}

	result = inet_pton(AF_INET, "255.255.255.0", vpn_ifr.ifr_addr.sa_data + sizeof(short));
	if(result < 1){
		perror("inet_pton \"255.255.255.0\" failed");
		return result;
	}

	result = ioctl(globals->vpn_fd, SIOCSIFNETMASK, &vpn_ifr);
	if(result < 0){
		perror("ioctl SIOCSIFNETMASK failed");
		return result;
	}

	vpn_ifr.ifr_mtu = globals->mtu;
	result = ioctl(globals->vpn_fd, SIOCSIFMTU, &vpn_ifr);
	if(result < 0){
		perror("ioctl SIOCSIFMTU failed");
		return result;
	}

	vpn_ifr.ifr_flags = IFF_UP | IFF_RUNNING;
	result = ioctl(globals->vpn_fd, SIOCSIFFLAGS, &vpn_ifr);
	if(result < 0){
		perror("ioctl SIOCSIFFLAGS failed");
		return result;
	}

	struct sockaddr_in s;
	memset(&s, 0, sizeof(s));

	s.sin_family = AF_INET;
	s.sin_port = htons(VPN_LISTEN_PORT);
	inet_aton(VPN_LISTEN_HOST, (struct in_addr*)&s.sin_addr.s_addr);

	result = bind(globals->vpn_fd, (struct sockaddr*)&s, sizeof(s));
	if(result < 0){
		perror("Failed to bind socket to "VPN_LISTEN_HOST);
		return result;
	}

	result = listen(globals->vpn_fd, 8);
	if(result < 0){
		perror("Failed to listen socket");
		return result;
	}

	return 1;
}

int main(){
	struct globals globals = {0};
	globals.mtu = TUN_MTU;

	int result = setup(&globals);
	if(result < 1){
		cleanup(&globals);
		return result;
	}

	char *buffer = alloca(globals.mtu + 1);
	if(buffer == NULL){
		perror("Failed to malloc buffer\n");
		cleanup(&globals);
		return errno;
	}

	for(;;){
		static int client_socket;
		client_socket = accept(globals.vpn_fd, NULL, 0);
		if(client_socket < 0){
			perror("Accept client socket failed");
			return 1;
		}

		result = read(client_socket, buffer, globals.mtu);
		if(result < 0){
			perror("Failed to read");
			return 1;
		}

		if(result > 0){
			buffer[result] = '\0';
			printf("read %i: %s\n", result, buffer);
		}

		result = shutdown(client_socket, SHUT_RDWR);
		if(result < 0){
			perror("Failed to shutdown client socket");
			return result;
		}

		result = close(client_socket);
		if(result < 0){
			perror("Close client_socket failed");
			return result;
		}
	}

	cleanup(&globals);
	return 0;
}
