CC=gcc
CFLAGS= -O3 -march=native -fstack-protector-all -Wall -Wextra -pedantic -g

export

.PHONY: all
all: server client

.PHONY: server
server: bin/vpn-server

.PHONY: client
client: bin/vpn-client

.PHONY: clean
clean:
	$(RM) bin/*


bin/vpn-server:
	$(CC) $(CFLAGS) -static -o bin/vpn-server src/vpn-server.c


bin/vpn-client:
	$(CC) $(CFLAGS) -static -o bin/vpn-client src/vpn-client.c

